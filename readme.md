DMN: DMN-Routing-Sample
=======================

### Problem Statement:

- The requirement is to have multiple DMN files for each client and to call them based on the client ID provided in the input. I think this approach will allow for better management and maintenance of the rules for each client.

### Solution:

- This application contains a decision table with the condition column as a number which is the clientId. The value passed in from the rules engine request is evaluated against the rows in the condition column. If the value match, then the call is routed based on the action column which will call a second DMN model. In the second DMN model, for the purpose of this demo, it is simply returning a true or false literal expression to show the second model is successfully invoked. 


#
#

### Testing client-id-sample:

The following tests were conducted on Red Hat Decision Manager 7.x. After build and deploy the kjar on business central, use postman to invoke the following url to access the engine.

Using Postman:

Set call to POST:

Set URL:

- http://localhost:8080/kie-server/services/rest/server/containers/router-sample_1.0.0-SNAPSHOT/dmn/models/sample


Select body and input:

- { "clientId": 100 }

You should receive response:
- "client router": true

Replacing "100" with any other number shall return false.


Note:
Set authentication if necessary for your environment.



